//
//  Color.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import SwiftUI

extension Color {
  
    init(hex: String) {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        
        self.init(
            .sRGB,
            red: Double(r) / 255,
            green: Double(g) / 255,
            blue:  Double(b) / 255,
            opacity: Double(a) / 255
        )
    }
    
    static var mainColor = Color("mainColor")
    static var lightGray = Color("lightGray")
    
    static func colorForIndex(_ index: Int) -> Color {
        switch index {
        case 0: return Color("mint")
        case 1: return Color("lightYellow")
        case 2: return Color("lightOrange")
        case 3: return Color("lightBlue")
        case 4: return Color("pinkColor")
        default: return Color("mint")
        }
    }
}
