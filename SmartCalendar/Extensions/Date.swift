//
//  Date.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import Foundation

extension Date {
    
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }

    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
    
    var isToday: Bool {
        let now = Date()
        return self.get(.day) == now.get(.day) && self.get(.month) == now.get(.month) && self.get(.year) == now.get(.year)
    }
    
    func equals(_ date: Date) -> Bool {
        return self.get(.day) == date.get(.day) && self.get(.month) == date.get(.month) && self.get(.year) == date.get(.year)
    }
    
    var time: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "de_DE")
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: self)
    }
    
    var dateForDay: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "de_DE")
        dateFormatter.dateFormat = "d.MM.YYYY"
        return dateFormatter.string(from: self)
    }
    
    var etString: String {
        var components = Calendar.current.dateComponents([.day], from: Date(), to: self)
        if self < Date() {
            components = Calendar.current.dateComponents([.day], from: self, to: Date())
        }
        
        if self.get(.day) == Date().get(.day) {
            return "40 + 0"
        } else if self < Date() {
            return "\(40 + (components.day ?? 0) / 7) + \((components.day ?? 0) % 7)"
        } else {
            return "\(39 - (components.day ?? 0) / 7) + \(6 - (components.day ?? 0) % 7)"
        }
    }
}
