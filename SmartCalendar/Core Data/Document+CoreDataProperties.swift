//
//  Document+CoreDataProperties.swift
//  
//
//  Created by Conrad Felgentreff on 28.06.21.
//
//

import Foundation
import CoreData


extension Document {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Document> {
        return NSFetchRequest<Document>(entityName: "Document")
    }

    @NSManaged public var id: UUID
    @NSManaged public var title: String
    @NSManaged public var text: String
    @NSManaged public var type: String
    @NSManaged public var appointment: Appointment
}
