//
//  Child+CoreDataProperties.swift
//  
//
//  Created by Conrad Felgentreff on 30.06.21.
//
//

import Foundation
import CoreData


extension Child {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Child> {
        return NSFetchRequest<Child>(entityName: "Child")
    }

    @NSManaged public var id: UUID
    @NSManaged public var firstName: String
    @NSManaged public var lastName: String
    @NSManaged public var birthDate: Date
    @NSManaged public var mother: Contact

}
