//
//  Appointment+CoreDataProperties.swift
//  
//
//  Created by Conrad Felgentreff on 18.06.21.
//
//

import Foundation
import CoreData


extension Appointment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Appointment> {
        return NSFetchRequest<Appointment>(entityName: "Appointment")
    }

    @NSManaged public var startDate: Date
    @NSManaged public var endDate: Date
    @NSManaged public var details: String?
    @NSManaged public var id: UUID
    @NSManaged public var title: String
    @NSManaged public var reminderOne: Int32
    @NSManaged public var reminderTwo: Int32
    @NSManaged public var origin: Contact
    @NSManaged public var document: Document?
}
