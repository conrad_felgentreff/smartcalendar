//
//  Contact+CoreDataProperties.swift
//  
//
//  Created by Conrad Felgentreff on 18.06.21.
//
//

import Foundation
import CoreData


extension Contact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contact> {
        return NSFetchRequest<Contact>(entityName: "Contact")
    }

    @NSManaged public var firstName: String
    @NSManaged public var id: UUID
    @NSManaged public var infos: String?
    @NSManaged public var lastName: String?
    @NSManaged public var imageData: Data?
    @NSManaged public var colorIndex: Int16
    @NSManaged public var birthDate: Date
    @NSManaged public var phone: String?
    @NSManaged public var email: String?
    @NSManaged public var gravida: Int16
    @NSManaged public var para: Int16
    @NSManaged public var et: Date
    @NSManaged public var bloodGroup: String
    @NSManaged public var rhesusFactor: Bool
    @NSManaged public var hbsAg: Bool
    @NSManaged public var gbs: Bool
    @NSManaged public var hiv: Bool
    @NSManaged public var createdAt: Date?
    @NSManaged public var appointments: NSSet?
    @NSManaged public var childs: NSSet?
    
    public var appointmentArray: [Appointment] {
        let set = self.appointments as? Set<Appointment> ?? []
        
        return set.sorted {
            $0.startDate < $1.startDate
        }
    }
    
    public var childsArray: [Child] {
        let set = self.childs as? Set<Child> ?? []
        
        return set.sorted {
            $0.birthDate < $1.birthDate
        }
    }
}

// MARK: Generated accessors for appointment
extension Contact {

    @objc(addAppointmentObject:)
    @NSManaged public func addToAppointment(_ value: Appointment)

    @objc(removeAppointmentObject:)
    @NSManaged public func removeFromAppointment(_ value: Appointment)

    @objc(addAppointment:)
    @NSManaged public func addToAppointment(_ values: NSSet)

    @objc(removeAppointment:)
    @NSManaged public func removeFromAppointment(_ values: NSSet)

}
