//
//  Reminder.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 25.06.21.
//

import Foundation

enum Reminder: CaseIterable {
    
    case none
    case now
    case fiveMin
    case tenMin
    case fifteenMin
    case thirtyMin
    case oneHour
    case twoHours
    case oneDay
    case twoDays
    case oneWeek
    
    var minutes: Int {
        switch self {
        case .none:
            return -1
        case .now:
            return 0
        case .fiveMin:
            return 5
        case .tenMin:
            return 10
        case .fifteenMin:
            return 15
        case .thirtyMin:
            return 30
        case .oneHour:
            return 60
        case .twoHours:
            return 120
        case .oneDay:
            return 1440
        case .twoDays:
            return 2880
        case .oneWeek:
            return 10080
        }
    }
    
    var title: String {
        switch self {
        case .none:
            return "Ohne"
        case .now:
            return "Zum Ereigniszeitpunkt"
        case .fiveMin:
            return "5 Minuten vorher"
        case .tenMin:
            return "10 Minuten vorher"
        case .fifteenMin:
            return "15 Minuten vorher"
        case .thirtyMin:
            return "30 Minuten vorher"
        case .oneHour:
            return "1 Stunde vorher"
        case .twoHours:
            return "2 Stunden vorher"
        case .oneDay:
            return "1 Tag vorher"
        case .twoDays:
            return "2 Tage vorher"
        case .oneWeek:
            return "1 Woche vorher"
        }
    }
    
    var notificationMessage: String {
        switch self {
        case .none:
            return ""
        case .now:
            return "Jetzt"
        case .fiveMin:
            return "in 5 Minuten"
        case .tenMin:
            return "in 10 Minuten"
        case .fifteenMin:
            return "in 15 Minuten"
        case .thirtyMin:
            return "in 30 Minuten"
        case .oneHour:
            return "in einer Stunde"
        case .twoHours:
            return "in zwei Stunden"
        case .oneDay:
            return "Morgen"
        case .twoDays:
            return "Übermorgen"
        case .oneWeek:
            return "in einer Woche"
        }
    }
    
    static func getReminderFromInt(_ int: Int) -> Reminder {
        switch int {
        case -1:
            return .none
        case 0:
            return .now
        case 5:
            return .fiveMin
        case 10:
            return .tenMin
        case 15:
            return .fifteenMin
        case 30:
            return .thirtyMin
        case 60:
            return .oneHour
        case 120:
            return .twoHours
        case 1440:
            return .oneDay
        case 2880:
            return .twoDays
        case 10080:
            return .oneWeek
        default:
            return .none
        }
    }
}

