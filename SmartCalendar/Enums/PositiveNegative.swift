//
//  PositiveNegative.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 25.06.21.
//

import Foundation

enum PositiveNegative: String, CaseIterable {
    case positive = "Positiv"
    case negative = "Negativ"
    
    static func getPositiveNegative(fromBool bool: Bool) -> PositiveNegative {
        return bool ? .positive : .negative
    }
}
