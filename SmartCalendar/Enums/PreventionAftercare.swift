//
//  PreventionAftercare.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 28.06.21.
//

import Foundation

enum PreventionAftercare: String, CaseIterable {
    case prevention = "Vorsorge"
    case afterCare = "Nachsorge"
}
