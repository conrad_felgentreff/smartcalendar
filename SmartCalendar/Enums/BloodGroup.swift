//
//  BloodGroup.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 25.06.21.
//

import Foundation

enum BloodGroup: String, CaseIterable {
    case a = "A"
    case b = "B"
    case ab = "AB"
    case zero = "0"
}
