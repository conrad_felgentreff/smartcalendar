//
//  ContentView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import SwiftUI
import Introspect

struct ContentView: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    
    @State var index = 0
    @State var showSideMenu = false
    
    var body: some View {
            
            TabView(selection: self.$index) {
                
                NavigationView {
                    
                    MainCalendarView(showSideMenu: self.$showSideMenu)
                        .addNavBar(index: self.$index, showSideMenu: self.$showSideMenu)
                }
                .tag(0)
                .navigationViewStyle(StackNavigationViewStyle())
                
                NavigationView {
                    
                    MainContactView(showSideMenu: self.$showSideMenu)
                        .addNavBar(index: self.$index, showSideMenu: self.$showSideMenu)
                }
                .tag(1)
                .navigationViewStyle(StackNavigationViewStyle())
            }
            .introspectTabBarController { tabBarController in
                tabBarController.tabBar.isHidden = true
            }
            .addSideMenu(show: self.$showSideMenu, index: self.$index)
            .onAppear {
                UITableView.appearance().backgroundColor = .clear
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
                    if success {
                        print("All set!")
                    } else if let error = error {
                        print(error.localizedDescription)
                    }
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
