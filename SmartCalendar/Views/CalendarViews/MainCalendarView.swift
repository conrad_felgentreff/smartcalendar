//
//  MainCalendarView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import SwiftUI

struct MainCalendarView: View {
    
    @Environment(\.calendar) var calendar
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(
        entity: Appointment.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Appointment.startDate, ascending: true),
        ]
    ) var appointments: FetchedResults<Appointment>
    
    @Binding var showSideMenu: Bool
    
    var threeYears = DateInterval(start: Calendar.current.date(byAdding: .year, value: -1, to: Date()) ?? Date(), end: Calendar.current.date(byAdding: .year, value: 2, to: Date()) ?? Date())
    
    @State var showSheet = false
    @State var selectedDate: Date?
    var days = ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"]
    @State var showAddNewContactView = false
    
    var body: some View {
        
        GeometryReader { geometry in
            
            ZStack {
                
                NavigationLink(destination: NewContactView(), isActive: self.$showAddNewContactView) {
                    Text("")
                }
             
                CalendarView(interval: threeYears) { date in
                    
                    Button {
                        self.selectedDate = date
                    } label: {
                        CalendarCell(date: date)
                            .frame(width: geometry.size.width / 7, height: 100)
                    }
                }
                
                VStack {
                    
                    HStack(spacing: 0) {
                        
                        ForEach(self.days, id: \.self) { day in
                            
                            Text(String(day.first!))
                                .font(.system(size: 10, weight: .bold, design: .default))
                                .foregroundColor(.black)
                                .padding(2)
                                .frame(width: geometry.size.width / 7)
                        }
                    }
                    .background(Color.gray
                                    .opacity(0.05)
                                    .edgesIgnoringSafeArea(.horizontal)
                    )
                    
                    Spacer()
                }
            }
        }
        .onChange(of: self.selectedDate){ date in
            if date != nil {
                self.showSheet = true
            }
        }
        .fullScreenCover(isPresented: self.$showSheet) {
            DetailsView(date: self.selectedDate ?? Date(), showSheet: self.$showSheet)
                .onDisappear {
                    self.selectedDate = nil
                }
        }
        .background(Color.lightGray.edgesIgnoringSafeArea(.horizontal))
        .ignoresSafeArea(edges: .bottom)
        .navigationBarTitle("Kalender", displayMode: .inline)
    }
}
