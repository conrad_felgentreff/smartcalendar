//
//  EditAppointmentView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 25.06.21.
//

import SwiftUI

struct EditAppointmentView: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(
        entity: Contact.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Contact.lastName, ascending: true),
        ]
    ) var contacts: FetchedResults<Contact>
    
    @ObservedObject var appointment: Appointment
    
    @Binding var show: Bool
    @Binding var showDetailsView: Bool
    
    @State var title = ""
    @State var contactIndex = 0
    @State var details = ""
    @State var startDate: Date
    @State var endDate = Calendar.current.date(byAdding: .hour, value: 1, to: Date()) ?? Date()
    @State var reminderOne: Reminder = .none
    @State var reminderTwo: Reminder = .none
    var valuesChanged: Bool {
        self.appointment.title != self.title || self.appointment.origin != self.contacts[self.contactIndex] || (self.appointment.details ?? "") != self.details || self.appointment.startDate != self.startDate || self.appointment.endDate != self.endDate || self.appointment.reminderOne != self.reminderOne.minutes || self.appointment.reminderTwo != self.reminderTwo.minutes
    }
    @State var showDismissAlert = false
    @State var showMissingDataAlert = false
    @State var showNoPermissionsAlert = false
    @State var showDeleteAlert = false
    
    init(appointment: Appointment, show: Binding<Bool>, showDetailsView: Binding<Bool>) {
        self.appointment = appointment
        self._show = show
        self._showDetailsView = showDetailsView
        self._title = State(initialValue: appointment.title)
        self._startDate = State(initialValue: appointment.startDate)
        self._endDate = State(initialValue: appointment.endDate)
        self._details = State(initialValue: appointment.details ?? "")
        self._reminderOne = State(initialValue: Reminder.getReminderFromInt(Int(appointment.reminderOne)))
        self._reminderTwo = State(initialValue: Reminder.getReminderFromInt(Int(appointment.reminderTwo)))
    }
    
    var body: some View {
        
        NavigationView {
            
            Form {
                
                Section {
                    
                    TextField("Titel", text: self.$title)
                }
                
                Section {
                    
                    HStack {
                        
                        Text("Kontakt")
                        
                        Spacer()
                        
                        Picker("                \(self.contacts[self.contactIndex].firstName) \(self.contacts[self.contactIndex].lastName ?? "")", selection: self.$contactIndex.animation(.spring())) {
                            
                            ForEach(0..<self.contacts.count) { index in
                                
                                Text("\(self.contacts[index].firstName) \(self.contacts[index].lastName ?? "")")
                            }
                        }
                        .pickerStyle(MenuPickerStyle())
                        .animation(.none)
                    }
                }
                
                Section {
                    
                    DatePicker("Beginn", selection: self.$startDate, displayedComponents: .hourAndMinute)
                    
                    DatePicker("Ende", selection: self.$endDate, in: self.startDate..., displayedComponents: .hourAndMinute)
                }
                
                Section {
                    
                    HStack {
                        
                        Text("Erinnerung")
                        
                        Spacer()
                        
                        Picker("                \(self.reminderOne.title)", selection: self.$reminderOne.animation(.spring())) {
                            
                            ForEach(Reminder.allCases.filter { self.reminderTwo != .none ? $0 != self.reminderTwo : true }, id: \.self) { reminder in
                                
                                Text(reminder.title)
                                    .lineLimit(1)
                            }
                        }
                        .pickerStyle(MenuPickerStyle())
                        .animation(.none)
                    }
                    
                    if self.reminderOne != .none {
                        
                        HStack {
                            
                            Text("2. Erinnerung")
                            
                            Spacer()
                            
                            Picker("                \(self.reminderTwo.title)", selection: self.$reminderTwo) {
                                
                                ForEach(Reminder.allCases.filter { $0 != self.reminderOne }, id: \.self) { reminder in
                                    
                                    Text(reminder.title)
                                        .lineLimit(1)
                                }
                            }
                            .pickerStyle(MenuPickerStyle())
                            .animation(.none)
                        }
                        .onDisappear {
                            self.reminderTwo = .none
                        }
                    }
                }
                .alert(isPresented: self.$showNoPermissionsAlert) {
                    noPermissionsAlert {
                        self.reminderOne = .none
                        self.reminderTwo = .none
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.saveAppointment()
                        }
                    }
                }
                
                Section {
                    
                    ZStack {
                        
                        if self.details.isEmpty {
                            
                            VStack {
                                
                                HStack {
                                    
                                    Text("Details (optional)")
                                        .padding(.vertical, 10)
                                        .padding(.horizontal, 5)
                                        .opacity(0.25)
                                    
                                    Spacer()
                                }
                                
                                Spacer()
                            }
                        }
                        
                        TextEditor(text: self.$details)
                            .frame(minHeight: 200)
                    }
                }
                
                Section {
                    
                    HStack {
                        
                        Spacer()
                        
                        Button {
                            self.showDeleteAlert = true
                        } label: {
                            Text("Termin löschen")
                                .foregroundColor(.red)
                        }
                        
                        Spacer()
                    }
                }
                .actionSheet(isPresented: self.$showDeleteAlert) {
                    ActionSheet(title: Text("Termin wirklich löschen?"),
                                buttons: [
                                    .cancel(Text("Abbrechen")),
                                    .destructive(Text("Termin löschen"), action: {
                                        self.deleteAppointment()
                                    })
                                ])
                }
            }
            .background(
                ZStack {
                    Color.white
                    
                    Color.colorForIndex(Int(self.contacts[self.contactIndex].colorIndex))
                        .opacity(0.4)
                }
                .edgesIgnoringSafeArea(.all)
            )
            .onChange(of: self.startDate) { startDate in
                if startDate > self.endDate {
                    self.endDate = startDate
                }
            }
            .navigationBarTitle("", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button {
                                        if self.valuesChanged {
                                            self.showDismissAlert = true
                                        } else {
                                            self.show = false
                                        }
                                    } label: {
                                        
                                        Text("Abbrechen")
                                            .font(.system(size: 17, weight: .regular, design: .default))
                                    }, trailing:
                                        Button {
                                            if self.title != "" {
                                                self.endAction()
                                            } else {
                                                self.showMissingDataAlert = true
                                            }
                                        } label: {
                                            
                                            Text("Aktualisieren")
                                        })
        }
        .alert(isPresented: self.$showMissingDataAlert) {
            self.missingDataAlert()
        }
        .actionSheet(isPresented: self.$showDismissAlert) {
            ActionSheet(
                title: Text("Willst du deine Änderungen wirklich verwerfen?"),
                buttons: [
                    .cancel(Text("Weiter bearbeiten")),
                    .destructive(Text("Änderungen verwerfen"), action: {
                        self.show = false
                    })
                ]
            )
        }
        .onAppear {
            for (index, contact) in self.contacts.enumerated() {
                if contact.id == appointment.origin.id {
                    self.contactIndex = index
                }
            }
        }
        .edgesIgnoringSafeArea(.all)
        .introspectViewController {
            $0.isModalInPresentation = self.valuesChanged
        }
    }
    
    func missingDataAlert() -> Alert {
        
        Alert(title: Text("Fehlende Daten"), message: Text("Bitte gebe einen Titel an."), dismissButton: .cancel(Text("Ok")))
    }
    
    func endAction() {
        if self.reminderOne != .none {
            self.requestReminders()
        } else {
            saveAppointment()
        }
    }
    
    func saveAppointment() {
        
        self.appointment.title = self.title
        self.appointment.origin = self.contacts[self.contactIndex]
        self.appointment.startDate = self.startDate
        self.appointment.endDate = self.endDate
        self.appointment.details = self.details != "" ? self.details : nil
        self.appointment.reminderOne = Int32(self.reminderOne.minutes)
        self.appointment.reminderTwo = Int32(self.reminderTwo.minutes)
        PersistenceController.shared.save()
        
        NotificationHelper.setNotifications(for: self.appointment)
        
        self.show = false
    }
    
    func requestReminders() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            if settings.authorizationStatus == .denied {
                self.showNoPermissionsAlert = true
            } else {
                self.saveAppointment()
            }
        }
    }
    
    func deleteAppointment() {
        self.managedObjectContext.delete(self.appointment)
        
        NotificationHelper.removeNotifications(for: self.appointment)
        
        PersistenceController.shared.save()
        self.show = false
        self.showDetailsView = false
    }
}
