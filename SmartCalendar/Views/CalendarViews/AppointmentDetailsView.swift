//
//  AppointmentDetailsView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 24.06.21.
//

import SwiftUI

struct AppointmentDetailsView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @ObservedObject var appointment: Appointment
    @Binding var showDetails: Bool
    @State var showEditView = false
    
    var body: some View {
        
        Form {
            
            Section {
                
                Text("\(self.appointment.title)")
            }
            
            Section {
                
                NavigationLink(destination: ContactDetailsView(contact: self.appointment.origin, showDetailsView: self.$showDetails)) {
                    
                    Text("\(self.appointment.origin.firstName) \(self.appointment.origin.lastName ?? "")")
                        .bold()
                }
            }
            
            Section {
                
                HStack {
                    
                    Text("Beginn")
                    
                    Spacer()
                    
                    Text("\(self.appointment.startDate.time)")
                        .foregroundColor(.gray)
                }
                
                HStack {
                    
                    Text("Beginn")
                    
                    Spacer()
                    
                    Text("\(self.appointment.endDate.time)")
                        .foregroundColor(.gray)
                }
            }
            
            Section {
                
                HStack {
                    
                    Text("Erinnerung")
                    
                    Spacer()
                    
                    Text("\(Reminder.getReminderFromInt(Int(self.appointment.reminderOne)).title)")
                        .foregroundColor(.gray)
                }
                
                if self.appointment.reminderTwo != -1 {
                    
                    HStack {
                        
                        Text("2. Erinnerung")
                        
                        Spacer()
                        
                        Text("\(Reminder.getReminderFromInt(Int(self.appointment.reminderTwo)).title))")
                            .foregroundColor(.gray)
                    }
                }
            }
            
            Section(header: Text("Details")) {
                
                VStack {
                    
                    Text(self.appointment.details ?? "Keine Details")
                    
                    Spacer()
                }
                .frame(minHeight: 150)
                .foregroundColor(.gray)
            }
        }
        .background(
            ZStack {
                Color.white
                
                Color.colorForIndex(Int(self.appointment.origin.colorIndex))
                    .opacity(0.4)
            }
            .edgesIgnoringSafeArea(.all)
        )
        .sheet(isPresented: self.$showEditView, content: {
            EditAppointmentView(appointment: self.appointment, show: self.$showEditView, showDetailsView: self.$showDetails)
        })
        .navigationBarBackButtonHidden(true)
        .toolbar {
            
            ToolbarItem(placement: .navigationBarLeading) {
                
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image(systemName: "chevron.backward")
                }
            }
            
            ToolbarItem(placement: .navigationBarTrailing) {
                
                Button(action: {
                    self.showEditView = true
                }) {
                    Text("Bearbeiten")
                }
            }
        }
        .edgesIgnoringSafeArea(.bottom)
    }
}
