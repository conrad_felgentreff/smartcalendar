//
//  NewAppointmentView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 22.06.21.
//

import SwiftUI
import UserNotifications

struct NewAppointmentView: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(
        entity: Contact.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Contact.lastName, ascending: true),
        ]
    ) var contacts: FetchedResults<Contact>
    @Binding var showSheet: Bool
    
    @State var title = ""
    @State var contactIndex = 0
    @State var details = ""
    @State var startDate: Date
    @State var endDate = Calendar.current.date(byAdding: .hour, value: 1, to: Date()) ?? Date()
    @State var reminderOne: Reminder = .none
    @State var reminderTwo: Reminder = .none
    @State var valuesChanged = false
    @State var showDismissAlert = false
    @State var showMissingDataAlert = false
    @State var showNoPermissionsAlert = false
    @State var appointmentUuid = UUID()
    
    init(date: Date, showSheet: Binding<Bool>) {
        self._showSheet = showSheet
        
        //Setting up start and endDate
        var dateComponents: DateComponents? = Calendar.current.dateComponents([.hour], from: Date())
        dateComponents?.day = date.get(.day)
        dateComponents?.month = date.get(.month)
        dateComponents?.year = date.get(.year)
        if dateComponents?.hour == 0 {
            dateComponents?.hour = 23
        }
        
        let startDate = Calendar.current.date(from: dateComponents ?? DateComponents()) ?? date
        self._startDate = State(initialValue: startDate)
        self._endDate = State(initialValue: Calendar.current.date(byAdding: .hour, value: 1, to: startDate) ?? startDate)
    }
    
    var body: some View {
        
        NavigationView {
            
            Form {
                
                Section {
                    
                    TextField("Titel", text: self.$title)
                }
                
                Section {
                    
                    HStack {
                        
                        Text("Kontakt")
                        
                        Spacer()
                        
                        Picker("                \(self.contacts[self.contactIndex].firstName) \(self.contacts[self.contactIndex].lastName ?? "")", selection: self.$contactIndex.animation(.spring())) {
                            
                            ForEach(0..<self.contacts.count) { index in
                                
                                Text("\(self.contacts[index].firstName) \(self.contacts[index].lastName ?? "")")
                            }
                        }
                        .pickerStyle(MenuPickerStyle())
                        .animation(.none)
                    }
                }
                
                Section {
                    
                    DatePicker("Beginn", selection: self.$startDate, displayedComponents: .hourAndMinute)
                    
                    DatePicker("Ende", selection: self.$endDate, in: self.startDate..., displayedComponents: .hourAndMinute)
                }
                
                Section {
                    
                    HStack {
                        
                        Text("Erinnerung")
                        
                        Spacer()
                        
                        Picker("                \(self.reminderOne.title)", selection: self.$reminderOne.animation(.spring())) {
                            
                            ForEach(Reminder.allCases.filter { self.reminderTwo != .none ? $0 != self.reminderTwo : true }, id: \.self) { reminder in
                                
                                Text(reminder.title)
                                    .lineLimit(1)
                            }
                        }
                        .pickerStyle(MenuPickerStyle())
                        .animation(.none)
                    }
                    
                    if self.reminderOne != .none {
                        
                        HStack {
                            
                            Text("2. Erinnerung")
                            
                            Spacer()
                            
                            Picker("                \(self.reminderTwo.title)", selection: self.$reminderTwo) {
                                
                                ForEach(Reminder.allCases.filter { $0 != self.reminderOne }, id: \.self) { reminder in
                                    
                                    Text(reminder.title)
                                        .lineLimit(1)
                                }
                            }
                            .pickerStyle(MenuPickerStyle())
                            .animation(.none)
                        }
                        .onDisappear {
                            self.reminderTwo = .none
                        }
                    }
                }
                .alert(isPresented: self.$showNoPermissionsAlert) {
                    noPermissionsAlert {
                        self.reminderOne = .none
                        self.reminderTwo = .none
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.saveAppointment()
                        }
                    }
                }
                
                Section {
                    
                    ZStack {
                        
                        if self.details.isEmpty {
                            
                            VStack {
                                
                                HStack {
                                    
                                    Text("Details (optional)")
                                        .padding(.vertical, 10)
                                        .padding(.horizontal, 5)
                                        .opacity(0.25)
                                    
                                    Spacer()
                                }
                                
                                Spacer()
                            }
                        }
                        
                        TextEditor(text: self.$details)
                            .frame(minHeight: 200)
                    }
                }
            }
            .background(
                ZStack {
                    Color.white
                    
                    Color.colorForIndex(Int(self.contacts[self.contactIndex].colorIndex))
                        .opacity(0.4)
                }
                .edgesIgnoringSafeArea(.all)
            )
            .actionSheet(isPresented: self.$showDismissAlert) {
                ActionSheet(
                    title: Text("Willst du den Termin wirklich verwerfen?"),
                    buttons: [
                        .cancel(Text("Weiter bearbeiten")),
                        .destructive(Text("Termin verwerfen"), action: {
                            self.showSheet = false
                        })
                    ]
                )
            }
            .alert(isPresented: self.$showMissingDataAlert) {
                self.missingDataAlert()
            }
            .onChange(of: self.title) { _ in
                self.valuesChanged = true
            }
            .onChange(of: self.startDate) { _ in
                self.valuesChanged = true
            }
            .onChange(of: self.endDate) { _ in
                self.valuesChanged = true
            }
            .onChange(of: self.details) { _ in
                self.valuesChanged = true
            }
            .onChange(of: self.contactIndex) { _ in
                self.valuesChanged = true
            }
            .onAppear {
                self.contactIndex = Int.random(in: 0..<self.contacts.count)
            }
            .navigationBarTitle("Neuer Termin", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button {
                                        if self.valuesChanged {
                                            self.showDismissAlert = true
                                        } else {
                                            self.showSheet = false
                                        }
                                    } label: {
                                        
                                        Text("Abbrechen")
                                            .font(.system(size: 17, weight: .regular, design: .default))
                                    }, trailing:
                                        Button {
                                            if self.title != "" {
                                                self.endAction()
                                            } else {
                                                self.showMissingDataAlert = true
                                            }
                                        } label: {
                                            
                                            Text("Speichern")
                                        })
        }
        .edgesIgnoringSafeArea(.all)
        .introspectViewController {
            $0.isModalInPresentation = self.valuesChanged
        }
    }
    
    func missingDataAlert() -> Alert {
        
        Alert(title: Text("Fehlende Daten"), message: Text("Bitte gebe einen Titel an."), dismissButton: .cancel(Text("Ok")))
    }
    
    func endAction() {
        if self.reminderOne != .none {
            self.requestReminders()
        } else {
            saveAppointment()
        }
    }
    
    func saveAppointment() {
        
        let newAppointment = Appointment(context: self.managedObjectContext)
        newAppointment.id = self.appointmentUuid
        newAppointment.title = self.title
        newAppointment.origin = self.contacts[self.contactIndex]
        newAppointment.startDate = self.startDate
        newAppointment.endDate = self.endDate
        newAppointment.details = self.details != "" ? self.details : nil
        newAppointment.reminderOne = Int32(self.reminderOne.minutes)
        newAppointment.reminderTwo = Int32(self.reminderTwo.minutes)
        PersistenceController.shared.save()
        
        NotificationHelper.setNotifications(for: newAppointment)
        
        self.showSheet = false
    }
    
    func requestReminders() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            if settings.authorizationStatus == .denied {
                self.showNoPermissionsAlert = true
            } else {
                self.saveAppointment()
            }
        }
    }
}
