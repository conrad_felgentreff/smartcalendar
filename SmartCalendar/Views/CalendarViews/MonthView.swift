//
//  MonthView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import SwiftUI

struct MonthView<DateView>: View where DateView: View {
    @Environment(\.calendar) var calendar

    let month: Date
    let content: (Date) -> DateView
    var monthString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        dateFormatter.locale = Locale(identifier: "de_DE")
        return dateFormatter.string(from: self.month)
    }
    var year: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        dateFormatter.locale = Locale(identifier: "de_DE")
        return dateFormatter.string(from: self.month.addingTimeInterval(15 * 86400))
    }

    init(
        month: Date,
        @ViewBuilder content: @escaping (Date) -> DateView
    ) {
        self.month = month
        self.content = content
    }

    private var weeks: [Date] {
        guard
            let monthInterval = calendar.dateInterval(of: .month, for: month)
            else { return [] }
        return calendar.generateDates(
            inside: monthInterval,
            matching: DateComponents(hour: 0, minute: 0, second: 0, weekday: Calendar.current.firstWeekday)
        )
    }

    var body: some View {
        VStack(spacing: 0) {
            
            HStack {
                
                Text(self.monthString)
                    .font(.system(size: 30, weight: .bold, design: .default))
                
                Spacer()
                    
                Text(self.year)
                    .font(.system(size: 30, weight: .thin, design: .default))
            }
            .padding(10)
            ForEach(weeks, id: \.self) { week in
                WeekView(week: week, content: self.content)
            }
        }
    }
}
