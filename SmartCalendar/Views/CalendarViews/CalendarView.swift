//
//  CalendarView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import SwiftUI

struct CalendarView<DateView>: View where DateView: View {
    
    @Environment(\.calendar) var calendar

    let interval: DateInterval
    let content: (Date) -> DateView
    
    @State private var scrollTarget: Date?

    init(
        interval: DateInterval,
        @ViewBuilder content: @escaping (Date) -> DateView
    ) {
        self.interval = interval
        self.content = content
    }

    private var months: [Date] {
        calendar.generateDates(
            inside: interval,
            matching: DateComponents(day: 1, hour: 0, minute: 0, second: 0)
        )
    }

    var body: some View {
        
        ZStack {
            
            ScrollView(.vertical, showsIndicators: false) {
                
                ScrollViewReader { value in
                    
                    Text("")
                        .onAppear {
                            value.scrollTo(months[12], anchor: .top)
                        }
                    
                    LazyVStack {
                        ForEach(months, id: \.self) { month in
                            MonthView(month: month, content: self.content)
                                .padding(.top)
                        }
                    }
                    .padding(.bottom, 50)
                    .onChange(of: self.scrollTarget) { scrollTarget in
                        if let target = scrollTarget {
                            
                            self.scrollTarget = nil
                            
                            withAnimation {
                                value.scrollTo(target, anchor: .top)
                            }
                        }
                    }
                }
            }
            
            VStack {
                
                Spacer()
                
                HStack {
                    
                    Spacer()
                    
                    Button {
                        
                        self.scrollTarget = months[12]
                    } label: {
                        Image(systemName: "calendar.circle")
                            .font(.system(size: 55, weight: .light, design: .default))
                            .foregroundColor(.gray)
                            .padding(25)
                    }
                }
            }
        }
    }
}
