//
//  AppointmentCell.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 25.06.21.
//

import SwiftUI

struct AppointmentCell: View {
    
    @ObservedObject var appointment: Appointment
    @State var showDetails = false
    
    var body: some View {
        
        NavigationLink(destination: AppointmentDetailsView(appointment: appointment, showDetails: self.$showDetails), isActive: self.$showDetails) {
            
            HStack() {
                
                Text(appointment.title)
                    .bold()
                    .foregroundColor(.white)
                    .padding(10)
                
                Spacer()
                
                HStack(spacing: 2) {
                    
                    HStack(spacing: 1) {
                        
                        VStack {
                            
                            Text("-")
                        }
                        
                        VStack(alignment: .leading, spacing: 0) {
                            
                            Text(appointment.startDate.time)
                            
                            Text(appointment.endDate.time)
                        }
                    }
                    .font(.system(size: 14, weight: .regular, design: .default))
                    
                    Text("\(Image(systemName: "clock")) ")
                        .bold()
                        .padding(5)
                }
                .foregroundColor(.white)
                .padding(5)
            }
        }
        .listRowBackground(Color.colorForIndex(Int(self.appointment.origin.colorIndex)))
    }
}
