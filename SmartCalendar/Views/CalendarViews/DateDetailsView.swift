//
//  DetailsView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import SwiftUI

struct DetailsView: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest var appointments: FetchedResults<Appointment>
    @FetchRequest(
        entity: Contact.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Contact.lastName, ascending: true),
        ]
    ) var contacts: FetchedResults<Contact>
    
    var date: Date
    @Binding var showSheet: Bool
    @State var showAddNewAppointment = false
    @State var showMissingContactsAlert = false
    
    init(date: Date, showSheet: Binding<Bool>) {
        self.date = date
        self._showSheet = showSheet
        let dayStart = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date) ?? date
        let dayEnd = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: date) ?? date
        self._appointments = FetchRequest(
            entity: Appointment.entity(),
            sortDescriptors: [
                NSSortDescriptor(keyPath: \Appointment.startDate, ascending: true)
            ],
            predicate: NSPredicate(format: "startDate > %@ && startDate < %@", dayStart as NSDate, dayEnd as NSDate)
        )
    }
    
    var body: some View {
        
        NavigationView {
            
            VStack(spacing: 0) {
                
                VStack(spacing: 0) {
                    
                    if self.appointments.isEmpty {
                        
                        Text("Keine Termine")
                            .font(.system(size: 20, weight: .regular, design: .default))
                            .foregroundColor(.gray)
                            .padding(.top)
                    } else {
                        
                        List {
                            
                            ForEach(self.appointments, id: \.id) { appointment in
                                
                                AppointmentCell(appointment: appointment)
                            }
                        }
                    }
                }
                
                Spacer()
            }
            .alert(isPresented: self.$showMissingContactsAlert) {
                self.missingContactsAlert()
            }
            .navigationBarTitle(Text(self.date.dateForDay), displayMode: .inline)
            .navigationBarItems(leading: Button {
                self.showSheet = false
            } label: {
                
                Text("Schließen")
                    .font(.system(size: 17, weight: .regular, design: .default))
            }, trailing: Button {
                if !self.contacts.isEmpty {
                    self.showAddNewAppointment = true
                } else {
                    self.showMissingContactsAlert = true
                }
            } label: {
                
                Image(systemName: "plus")
                    .font(.system(size: 20))
                    .scaleEffect(1.1)
                    .foregroundColor(.black)
                    .padding(.vertical)
                    .padding(.leading)
            })
        }
        .sheet(isPresented: self.$showAddNewAppointment) {
            NewAppointmentView(date: self.date, showSheet: self.$showAddNewAppointment)
        }
        .background(Color.lightGray)
        .edgesIgnoringSafeArea(.bottom)
    }
    
    func missingContactsAlert() -> Alert {
        Alert(title: Text("Fehlende Kontakte"), message: Text("Du hast keine Kontakte, denen du einen Termin zuordnen könntest."), dismissButton: .cancel(Text("Ok")))
    }
}

struct DetailsView_Previews: PreviewProvider {
    static var previews: some View {
        DetailsView(date: Date(), showSheet: Binding.constant(true))
    }
}
