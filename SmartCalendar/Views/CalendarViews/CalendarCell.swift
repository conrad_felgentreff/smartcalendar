//
//  CalendarCell.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import SwiftUI

struct CalendarCell: View {
    
    @Environment(\.calendar) var calendar
    
    var date: Date
    @FetchRequest var appointments: FetchedResults<Appointment>
    
    init(date: Date) {
        self.date = date
        let dayStart = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: date) ?? date
        let dayEnd = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: date) ?? date
        self._appointments = FetchRequest(
            entity: Appointment.entity(),
            sortDescriptors: [
                NSSortDescriptor(keyPath: \Appointment.startDate, ascending: true)
            ],
            predicate: NSPredicate(format: "startDate > %@ && startDate < %@", dayStart as NSDate, dayEnd as NSDate)
        )
    }
    
    var body: some View {
        
        RoundedRectangle(cornerRadius: 5)
            .foregroundColor(date.isToday ? .mainColor : .white)
            .overlay(
                
                VStack {
                    
                    HStack {
                        
                        Text(String(self.calendar.component(.day, from: date)))
                            .font(.system(size: 10, weight: date.isToday ? .bold : .regular, design: .default))
                            .padding(4)
                            .foregroundColor(date.isToday ? .white : .gray)
                        
                        Spacer()
                    }
                    
                    VStack(spacing: 0) {
                        
                        ForEach(appointments.prefix(3), id: \.id) { appointment in
                            
                            HStack {
                                
                                Text(appointment.title ?? "Eintrag")
                                    .foregroundColor(.white)
                                    .font(.system(size: 10, weight: date.isToday ? .bold : .regular, design: .default))
                                    .lineLimit(1)
                                    .padding(2)
                                    .background(RoundedRectangle(cornerRadius: 4)
                                                    .foregroundColor(Color.colorForIndex(Int(appointment.origin.colorIndex))))
                                
                                Spacer(minLength: 0)
                            }
                            .padding(2)
                        }
                        
                        Spacer(minLength: 0)
                    }
                    
                    Spacer(minLength: 0)
                }
            )
            .padding(2)
    }
}
