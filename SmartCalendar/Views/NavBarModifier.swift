//
//  NavBarModifier.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 24.06.21.
//

import SwiftUI
import ContactsUI

struct NavBarModifier: ViewModifier {
    
    @Binding var index: Int
    @Binding var showSideMenu: Bool
    @State var selectedContact: CNContact?
    @State var showActionSheet = false
    @State var showContactPicker = false
    @State var showAddNewContactView = false
    
    func body(content: Content) -> some View {
        
        ZStack {
            
            NavigationLink(destination: NewContactView(inputImage: self.selectedContact?.imageData != nil ? UIImage(data: (self.selectedContact?.imageData)!) : nil, firstName: self.selectedContact?.givenName ?? "", lastName: self.selectedContact?.familyName ?? "", phone: (self.selectedContact?.phoneNumbers.first?.value)?.stringValue ?? "", email: ((self.selectedContact?.emailAddresses.first?.value) as String?) ?? "").onDisappear { self.selectedContact = nil }, isActive: self.$showAddNewContactView) {
                Text("")
            }
            
            content
        }
        .actionSheet(isPresented: self.$showActionSheet) {
            ActionSheet(title: Text("Neuer Kontakt"), buttons: [
                .cancel(Text("Abbrechen")),
                .default(Text("Neuen Kontakt erstellen"), action: {
                    self.showAddNewContactView = true
                }),
                .default(Text("Neuen Kontakt importieren"), action: {
                    self.showContactPicker = true
                })
            ])
        }
        .sheet(isPresented: self.$showContactPicker, onDismiss: self.loadContact) {
            ContactPicker(contact: self.$selectedContact)
        }
        .navigationBarItems(leading: Button(action: {
            withAnimation(.easeInOut) {
                self.showSideMenu = true
            }
        }, label: {
            Image(systemName: "line.horizontal.3")
                .resizable()
                .frame(height: 14)
                .scaleEffect(1.1)
                .foregroundColor(.black)
                .padding(.vertical)
                .padding(.trailing)
        }), trailing: Button(action: {
            self.showActionSheet = true
        }, label: {
            Image(systemName: "person.crop.circle.badge.plus")
                .font(.system(size: 24))
                .scaleEffect(1.1)
                .foregroundColor(.black)
                .padding(.vertical)
                .padding(.leading)
        }))
    }
    
    func loadContact() {
        guard let contact = self.selectedContact else { return }
        self.selectedContact = contact
        self.showAddNewContactView = true
    }
}
