//
//  SideMenuModifier.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 19.06.21.
//

import SwiftUI

struct SideMenuModifier: ViewModifier {
    
    @Binding var show: Bool
    @Binding var index: Int

    func body(content: Content) -> some View {
        
        GeometryReader { geometry in
            
            ZStack {
                
                content
                    .brightness(self.show ? -0.3 : 0)
                    .disabled(self.show)
                
                Color.clear
                    .contentShape(Rectangle())
                    .onTapGesture {
                        withAnimation(.easeInOut) {
                            self.show = false
                        }
                    }
                    .disabled(!self.show)
                    .opacity(self.show ? 1 : 0)
                
                HStack {
                    
                    SideMenu(showSideMenu: self.$show, index: self.$index)
                        .frame(width: 300)
                    
                    Spacer()
                }
                .offset(x: self.show ? 0 : -300 - geometry.safeAreaInsets.leading)
            }
        }
    }
}
