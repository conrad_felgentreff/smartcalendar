//
//  NewDocumentView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 28.06.21.
//

import SwiftUI

struct NewDocumentView: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    
    @ObservedObject var contact: Contact
    @Binding var show: Bool
    @State var appointmentIndex = 0
    @State var title = ""
    @State var details = ""
    @State var type: PreventionAftercare = .prevention
    var appointmentArray: [Appointment]
    
    init(contact: Contact, show: Binding<Bool>) {
        self.contact = contact
        self._show = show
        self.appointmentArray = contact.appointmentArray.compactMap { $0.document == nil && $0.startDate < Date() ? $0 : nil }
    }
    
    var body: some View {
        
        NavigationView {
            
            Form {
                
                Section {
                    
                    TextField("Titel", text: self.$title)
                }
                
                Section(header: Text("Kategorie")) {
                    
                    Picker("Kategorie", selection: self.$type) {
                        
                        ForEach(PreventionAftercare.allCases, id: \.self) { type in
                            
                            Text("\(type.rawValue)")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section {
                    
                    HStack {
                        
                        Text("Termin")
                        
                        Spacer()
                        
                        Picker("                \(self.appointmentArray[self.appointmentIndex].startDate.dateForDay), \(self.appointmentArray[self.appointmentIndex].startDate.time) - \(self.appointmentArray[self.appointmentIndex].endDate.time)", selection: self.$appointmentIndex.animation(.spring())) {
                            
                            ForEach(0..<self.appointmentArray.count) { index in
                                
                                Text("\(self.appointmentArray[index].startDate.dateForDay), \(self.appointmentArray[index].startDate.time) - \(self.appointmentArray[index].endDate.time)")
                            }
                        }
                        .pickerStyle(MenuPickerStyle())
                        .animation(.none)
                    }
                }
                
                Section {
                    
                    ZStack {
                        
                        if self.details.isEmpty {
                            
                            VStack {
                                
                                HStack {
                                    
                                    Text("Details")
                                        .padding(.vertical, 10)
                                        .padding(.horizontal, 5)
                                        .opacity(0.25)
                                    
                                    Spacer()
                                }
                                
                                Spacer()
                            }
                        }
                        
                        TextEditor(text: self.$details)
                            .frame(minHeight: 200)
                    }
                }
            }
            .background(
                ZStack {
                    Color.white
                    
                    Color.colorForIndex(Int(self.contact.colorIndex))
                        .opacity(0.4)
                }
                .edgesIgnoringSafeArea(.all)
            )
            .onAppear {
                self.appointmentIndex = Int.random(in: 0..<self.appointmentArray.count)
            }
            .navigationBarTitle("Neues Dokument", displayMode: .inline)
            .navigationBarItems(leading:
                                    Button {
                                        self.show = false
                                    } label: {
                                        
                                        Text("Abbrechen")
                                            .font(.system(size: 17, weight: .regular, design: .default))
                                    }, trailing:
                                        Button {
                                            self.saveDocument()
                                        } label: {
                                            
                                            Text("Speichern")
                                        })
        }
    }
    
    private func saveDocument() {
        let document = Document(context: self.managedObjectContext)
        document.id = UUID()
        document.title = self.title
        document.text = self.details
        document.type = self.type.rawValue
        document.appointment = self.appointmentArray[self.appointmentIndex]
        PersistenceController.shared.save()
        self.show = false
    }
}
