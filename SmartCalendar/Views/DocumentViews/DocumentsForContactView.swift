//
//  DocumentsForContactView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 28.06.21.
//

import SwiftUI

struct DocumentsForContactView: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    
    var contact: Contact
    private var documents: [Document] {
        self.contact.appointmentArray.compactMap { $0.document }
    }
    
    var body: some View {
        
        List {
            
            ForEach(self.documents, id: \.id) { document in
                
                Text(document.title)
                    .bold()
            }
        }
    }
}
