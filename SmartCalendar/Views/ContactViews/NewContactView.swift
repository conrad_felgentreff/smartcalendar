//
//  AddNewContactView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 18.06.21.
//

import SwiftUI

struct NewContactView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.managedObjectContext) var managedObjectContext
    
    @State var inputImage: UIImage?
    @State var firstName: String = ""
    @State var lastName: String = ""
    @State var birthDate: Date = Calendar.current.date(byAdding: .year, value: -25, to: Date()) ?? Date()
    @State private var age: Int?
    @State var phone: String = ""
    @State var email: String = ""
    @State private var gravida: Int = 0
    @State var para: Int = 0
    @State private var et: Date = Calendar.current.date(byAdding: .weekOfYear, value: 40, to: Date()) ?? Date()
    @State private var pregWeek: String?
    @State private var bloodGroup: BloodGroup = .a
    @State private var rhesusFactor: PositiveNegative = .positive
    @State private var hbsAg: PositiveNegative = .negative
    @State private var gbs: PositiveNegative = .negative
    @State private var hiv: PositiveNegative = .negative
    @State private var infos: String = ""
    @State private var showActionSheet = false
    @State private var sourceType: UIImagePickerController.SourceType = .photoLibrary
    @State private var showImagePicker = false
    @State private var showDismissAlert = false
    var shouldShowDismissAlert: Bool {
        self.inputImage != nil || self.firstName != "" || self.lastName != "" || self.phone != "" || self.email != "" || self.age != nil || self.pregWeek != nil || self.gravida != 0 || self.para != 0 || self.infos != ""
    }
    @State private var showMissingInputAlert = false
    var shouldShowMissingInputAlert: Bool {
        self.firstName == "" || self.age == nil || self.pregWeek == nil
    }
    @State private var selectedColorIndex: Int = 0
    var birthDateRange: ClosedRange<Date> {
        return (Calendar.current.date(byAdding: .year, value: -100, to: Date()) ?? Date())...(Calendar.current.date(byAdding: .year, value: -12, to: Date()) ?? Date())
    }
    
    var body: some View {
        
        Form {
            
            HStack {
                
                Spacer()
                
                if let uiImage = self.inputImage {
                    
                    VStack {
                        
                        Image(uiImage: uiImage)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .background(Color.white)
                            .clipShape(Circle())
                            .frame(height: 200)
                            .overlay(
                                
                                VStack {
                                    
                                    HStack {
                                        
                                        Spacer()
                                        
                                        Button {
                                            self.inputImage = nil
                                        } label: {
                                            
                                            Image(systemName: "xmark.circle.fill")
                                        }
                                        .foregroundColor(.gray)
                                        .background(Color.white)
                                        .font(.system(size: 25))
                                        .clipShape(Circle())
                                    }
                                    
                                    Spacer()
                                }
                                .padding()
                            )
                        
                        Button {
                            self.showActionSheet = true
                        } label: {
                            
                            Text("Bild ändern")
                                .font(.system(size: 17, weight: .regular, design: .default))
                        }
                    }
                } else {
                    
                    VStack {
                        
                        Image(systemName: "person.fill")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .padding(12)
                            .offset(y: 12)
                            .foregroundColor(.white)
                            .background(Color.gray)
                            .frame(height: 200)
                            .clipShape(Circle())
                            .overlay(Circle().strokeBorder(lineWidth: 5).foregroundColor(.gray))
                        
                        Button {
                            self.showActionSheet = true
                        } label: {
                            
                            Text("Bild hinzufügen (optional)")
                                .font(.system(size: 17, weight: .regular, design: .default))
                        }
                    }
                }
                
                Spacer()
            }
            .buttonStyle(BorderlessButtonStyle())
            .listRowBackground(Color.colorForIndex(self.selectedColorIndex).opacity(0.4).animation(.spring()))
            
            Section(header: Text("Farbe")) {
                
                HStack {
                    
                    ForEach(0..<5) { index in
                        
                        MiniColor(colorIndex: index, selectedColorIndex: self.$selectedColorIndex)
                        
                        if index != 4 {
                            Spacer()
                        }
                    }
                }
                .buttonStyle(BorderlessButtonStyle())
                .actionSheet(isPresented: self.$showActionSheet) {
                    ActionSheet(
                        title: Text("Foto hinzufügen"),
                        buttons: [
                            .cancel(Text("Abbrechen")),
                            .default(Text("Bild auswählen"), action: {
                                self.sourceType = .photoLibrary
                                self.showImagePicker = true
                            }),
                            .default(Text("Bild aufnehmen"), action: {
                                self.sourceType = .camera
                                self.showImagePicker = true
                            })
                        ]
                    )
                }
                .fullScreenCover(isPresented: $showImagePicker, onDismiss: loadImage) {
                    ImagePicker(image: self.$inputImage, sourceType: self.sourceType)
                        .edgesIgnoringSafeArea(.all)
                }
            }
            .listRowBackground(Color.white)
            
            Section(header: Text("Name")) {
                
                TextField("Vorname", text: self.$firstName)
                    .disableAutocorrection(true)
                
                TextField("Nachname", text: self.$lastName)
                    .disableAutocorrection(true)
                    
                    .alert(isPresented: self.$showMissingInputAlert) {
                        self.missingDataAlert()
                    }
            }
            
            Section(header: Text("Kontakt")) {
                
                TextField("Telefonnummer (optional)", text: self.$phone)
                    .keyboardType(.phonePad)
                
                TextField("Email-Adresse (optional)", text: self.$email)
                    .keyboardType(.emailAddress)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
            }
            
            Section {
                
                DatePicker("Geburtsdatum", selection: self.$birthDate, in: self.birthDateRange, displayedComponents: .date)
                    .environment(\.locale, Locale.init(identifier: "de_DE"))
                    .onChange(of: self.birthDate) { date in
                        let components = Calendar.current.dateComponents([.year], from: date, to: Date())
                        withAnimation {
                            self.age = components.year
                        }
                    }
                
                if let age = self.age {
                    
                    HStack {
                        
                        Spacer()
                        
                        Text("Alter: \(age)")
                    }
                    .opacity(0.25)
                }
            }
            
            Section(header: Text("Zur Schwangerschaft")) {
                
                DatePicker("ET", selection: self.$et, in: ...(Calendar.current.date(byAdding: .weekOfYear, value: 40, to: Date()) ?? Date()), displayedComponents: .date)
                    .environment(\.locale, Locale.init(identifier: "de_DE"))
                    .onChange(of: self.et) { date in
                        withAnimation {
                            self.pregWeek = date.etString
                        }
                    }
                
                if let pregWeek = self.pregWeek {
                    
                    HStack {
                        
                        Spacer()
                        
                        Text(pregWeek)
                    }
                    .opacity(0.25)
                }
            }
            
            Section {
                
                HStack {
                    
                    Text("Gravida")
                    
                    Spacer()
                    
                    Picker("                \(self.gravida)", selection: self.$gravida) {
                        ForEach(0..<21) { number in
                            Text("\(number)")
                        }
                    }
                    .pickerStyle(MenuPickerStyle())
                    .animation(.none)
                }
                
                HStack {
                    
                    Text("Para")
                    
                    Spacer()
                    
                    Picker("                \(self.para)", selection: self.$para) {
                        ForEach(0..<21) { number in
                            Text("\(number)")
                        }
                    }
                    .pickerStyle(MenuPickerStyle())
                    .animation(.none)
                }
            }
            
            Section(header: Text("Blutgruppe")) {
                
                Picker("Blutgruppe", selection: self.$bloodGroup) {
                    
                    ForEach(BloodGroup.allCases, id: \.self) { bloodGroup in
                        
                        Text("\(bloodGroup.rawValue)")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                
                Picker("Rhesusfaktor", selection: self.$rhesusFactor) {
                    
                    ForEach(PositiveNegative.allCases, id: \.self) { rhesus in
                        
                        Text("\(rhesus.rawValue)")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
            }
            
            Group {
                
                Section(header: Text("HBS AG")) {
                    
                    Picker("HBS AG", selection: self.$hbsAg) {
                        
                        ForEach(PositiveNegative.allCases.reversed(), id: \.self) { hbs in
                            
                            Text("\(hbs.rawValue)")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("GBS")) {
                    
                    Picker("GBS", selection: self.$gbs) {
                        
                        ForEach(PositiveNegative.allCases.reversed(), id: \.self) { gbs in
                            
                            Text("\(gbs.rawValue)")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("HIV")) {
                    
                    Picker("HIV", selection: self.$hiv) {
                        
                        ForEach(PositiveNegative.allCases.reversed(), id: \.self) { hiv in
                            
                            Text("\(hiv.rawValue)")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
            }
            
            Section {
                
                ZStack {
                    
                    if self.infos.isEmpty {
                        
                        VStack {
                            
                            HStack {
                                
                                Text("Infos (optional)")
                                    .padding(.vertical, 10)
                                    .padding(.horizontal, 5)
                                    .opacity(0.25)
                                
                                Spacer()
                            }
                            
                            Spacer()
                        }
                    }
                    
                    TextEditor(text: self.$infos)
                        .frame(minHeight: 200)
                }
            }
        }
        .background(
            ZStack {
                Color.white
                
                Color.colorForIndex(self.selectedColorIndex)
                    .opacity(0.4)
            })
        .onAppear {
            self.selectedColorIndex = Int.random(in: 0...4)
        }
        .actionSheet(isPresented: self.$showDismissAlert) {
            ActionSheet(
                title: Text("Willst du den Kontakt wirklich verwerfen?"),
                buttons: [
                    .cancel(Text("Weiter bearbeiten")),
                    .destructive(Text("Kontakt verwerfen"), action: {
                        self.presentationMode.wrappedValue.dismiss()
                    })
                ]
            )
        }
        .edgesIgnoringSafeArea(.all)
        .introspectNavigationController { navigationController in
            navigationController.interactivePopGestureRecognizer?.isEnabled = false
        }
        .navigationBarTitle("Neuer Kontakt")
        .navigationBarBackButtonHidden(true)
        .toolbar {
            
            ToolbarItem(placement: .navigationBarLeading) {
                Button(action: {
                    if self.shouldShowDismissAlert {
                        self.showDismissAlert = true
                    } else {
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }) {
                    Text("Abbrechen")
                }
            }
            
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                
                Button(action: {
                    if self.shouldShowMissingInputAlert {
                        self.showMissingInputAlert = true
                    } else {
                        self.addToCoreDataAndDismiss()
                    }
                }) {
                    Text("Hinzufügen")
                }
            }
        }
    }
    
    func loadImage() {
        guard let inputImage = self.inputImage else { return }
        self.inputImage = inputImage
    }
    
    func missingDataAlert() -> Alert {
        Alert(title: Text("Fehlende Daten"), message: Text("Bitte gebe alle benötigten Daten an."), dismissButton: .cancel(Text("Ok")))
    }
    
    func addToCoreDataAndDismiss() {
        let newContact = Contact(context: self.managedObjectContext)
        newContact.id = UUID()
        if let image = self.inputImage {
            let turnedImage = image.fixedOrientation()
            newContact.imageData = turnedImage?.pngData() ?? image.pngData()
        }
        newContact.firstName = self.firstName
        newContact.lastName = self.lastName != "" ? self.lastName : nil
        newContact.birthDate = self.birthDate
        newContact.phone = self.phone != "" ? self.phone.replacingOccurrences(of: " ", with: "") : nil
        newContact.email = self.email != "" ? self.email : nil
        newContact.et = self.et
        newContact.gravida = Int16(self.gravida)
        newContact.para = Int16(self.para)
        newContact.bloodGroup = self.bloodGroup.rawValue
        newContact.rhesusFactor = self.getBoolForPositiveNegative(self.rhesusFactor.rawValue)
        newContact.hbsAg = self.getBoolForPositiveNegative(self.hbsAg.rawValue)
        newContact.gbs = self.getBoolForPositiveNegative(self.gbs.rawValue)
        newContact.hiv = self.getBoolForPositiveNegative(self.hiv.rawValue)
        newContact.infos = self.infos != "" ? self.infos : nil
        newContact.colorIndex = Int16(self.selectedColorIndex)
        PersistenceController.shared.save()
        self.presentationMode.wrappedValue.dismiss()
    }
    
    func getBoolForPositiveNegative(_ string: String) -> Bool {
        string == "Positiv"
    }
}

struct AddNewContactView_Previews: PreviewProvider {
    static var previews: some View {
        NewContactView()
    }
}

struct MiniColor: View {
    
    var colorIndex: Int
    @Binding var selectedColorIndex: Int
    
    var body: some View {
        
        Button {
            
            withAnimation(.spring()) {
                self.selectedColorIndex = colorIndex
            }
        } label: {
            
            VStack {
                
                Color.colorForIndex(self.colorIndex)
                    .clipShape(RoundedRectangle(cornerRadius: 5))
                    .frame(width: 25, height: 25)
                    .scaleEffect(self.colorIndex == self.selectedColorIndex ? 1.4 : 1)
            }
        }
    }
}
