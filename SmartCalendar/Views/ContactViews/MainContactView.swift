//
//  MainContactView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 18.06.21.
//

import SwiftUI
import ContactsUI

struct MainContactView: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(
        entity: Contact.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Contact.firstName, ascending: true),
        ]
    ) var contacts: FetchedResults<Contact>
    
    @Binding var showSideMenu: Bool
    @State var showAddNewContactView = false
    @State var showContactPicker = false
    @State var showActionSheet = false
    @State var selectedContact: CNContact?
    var alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W", "X","Y", "Z"]
    
    var body: some View {
        
        ZStack {
            
            if self.contacts.isEmpty {
                
                VStack {
                    
                    Text("Keine Kontakte")
                        .font(.system(size: 20, weight: .regular, design: .default))
                        .foregroundColor(.gray)
                        .padding()
                    
                    Spacer()
                }
                .frame(maxWidth: .infinity)
            } else {
                
                List {
                    
                    ForEach(self.alphabet, id: \.self) { letter in
                        
                        if let contactsForLetter = self.contacts.filter { $0.lastName?.hasPrefix(letter) ?? $0.firstName.hasPrefix(letter) }, !contactsForLetter.isEmpty {
                            
                            Section(header: Text(letter)) {
                                
                                ForEach(contactsForLetter, id: \.id) { contact in
                                    
                                    ContactCell(contact: contact)
                                }
                            }
                        }
                    }
                    .onDelete(perform: self.deleteContact)
                }
            }
        }
        .background(Color.lightGray)
        .ignoresSafeArea(edges: .bottom)
        .navigationBarTitle("Kontakte", displayMode: .inline)
    }
    
    func deleteContact(at offsets: IndexSet) {
        for index in offsets {
            let contact = self.contacts[index]
            managedObjectContext.delete(contact)
            PersistenceController.shared.save()
        }
    }
}
