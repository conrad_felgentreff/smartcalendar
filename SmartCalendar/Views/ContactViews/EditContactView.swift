//
//  EditContactView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 22.06.21.
//

import SwiftUI

struct EditContactView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @Environment(\.managedObjectContext) var managedObjectContext
    @FetchRequest(
        entity: Contact.entity(),
        sortDescriptors: [
            NSSortDescriptor(keyPath: \Contact.firstName, ascending: true),
        ]
    ) var contacts: FetchedResults<Contact>
    
    @ObservedObject var contact: Contact
    
    @Binding var showDetailsView: Bool
    @State var inputImage: UIImage?
    @State var firstName: String = ""
    @State var lastName: String = ""
    @State var birthDate: Date = Calendar.current.date(byAdding: .year, value: -25, to: Date()) ?? Date()
    @State var age: Int
    @State var phone: String = ""
    @State var email: String = ""
    @State var gravida: Int = 0
    @State var para: Int = 0
    @State var et: Date = Calendar.current.date(byAdding: .weekOfYear, value: 40, to: Date()) ?? Date()
    @State var pregWeek: String
    @State var bloodGroup: BloodGroup = .a
    @State var rhesusFactor: PositiveNegative = .positive
    @State var hbsAg: PositiveNegative = .negative
    @State var gbs: PositiveNegative = .negative
    @State var hiv: PositiveNegative = .negative
    @State var infos: String = ""
    @State private var showActionSheet = false
    @State var sourceType: UIImagePickerController.SourceType = .photoLibrary
    @State private var showImagePicker = false
    @State var showDismissAlert = false
    var shouldShowDismissAlert: Bool {
        self.contact.imageData != self.inputImage?.fixedOrientation()?.pngData() || self.contact.colorIndex != self.selectedColorIndex || self.contact.firstName != self.firstName || (self.contact.lastName ?? "") != self.lastName || self.contact.birthDate != self.birthDate || (self.contact.phone ?? "") != self.phone || (self.contact.email ?? "") != self.email || self.contact.gravida != self.gravida || self.contact.para != self.para || self.contact.et != self.et || self.contact.bloodGroup != self.bloodGroup.rawValue || PositiveNegative.getPositiveNegative(fromBool: self.contact.rhesusFactor) != self.rhesusFactor || PositiveNegative.getPositiveNegative(fromBool: self.contact.hbsAg) != self.hbsAg || PositiveNegative.getPositiveNegative(fromBool: self.contact.gbs) != self.gbs || PositiveNegative.getPositiveNegative(fromBool: self.contact.hiv) != self.hiv || (self.contact.infos ?? "") != self.infos
    }
    @State var showMissingInputAlert = false
    var shouldShowMissingInputAlert: Bool {
        self.firstName == ""
    }
    @State var showDeleteAlert = false
    @State var selectedColorIndex: Int = 0
    var birthDateRange: ClosedRange<Date> {
        return (Calendar.current.date(byAdding: .year, value: -100, to: Date()) ?? Date())...(Calendar.current.date(byAdding: .year, value: -12, to: Date()) ?? Date())
    }
    
    init(contact: Contact, showDetailsView: Binding<Bool>) {
        self.contact = contact
        self._showDetailsView = showDetailsView
        if let imageData = contact.imageData {
            self._inputImage = State(initialValue: UIImage(data: imageData as Data) ?? nil)
        }
        self._selectedColorIndex = State(initialValue: Int(contact.colorIndex))
        self._firstName = State(initialValue: contact.firstName)
        self._lastName = State(initialValue: contact.lastName ?? "")
        self._birthDate = State(initialValue: contact.birthDate)
        var components = Calendar.current.dateComponents([.year], from: contact.birthDate, to: Date())
        self._age = State(initialValue: components.year ?? 0)
        self._phone = State(initialValue: contact.phone ?? "")
        self._email = State(initialValue: contact.email ?? "")
        self._et = State(initialValue: contact.et)
        components = Calendar.current.dateComponents([.day], from: Date(), to: contact.et)
        if contact.et == Date() {
            self._pregWeek = State(initialValue: "40 + 0")
        } else {
            self._pregWeek = State(initialValue: "\(39 - (components.day ?? 0) / 7) + \(6 - (components.day ?? 0) % 7)")
        }
        self._gravida = State(initialValue: Int(contact.gravida))
        self._para = State(initialValue: Int(contact.para))
        switch contact.bloodGroup {
        case "A": self._bloodGroup = State(initialValue: .a)
        case "B": self._bloodGroup = State(initialValue: .b)
        case "AB": self._bloodGroup = State(initialValue: .ab)
        case "0": self._bloodGroup = State(initialValue: .zero)
        default: self._bloodGroup = State(initialValue: .a)
        }
        self._rhesusFactor = State(initialValue: contact.rhesusFactor ? .positive : .negative)
        self._hbsAg = State(initialValue: contact.hbsAg ? .positive : .negative)
        self._gbs = State(initialValue: contact.gbs ? .positive : .negative)
        self._hiv = State(initialValue: contact.hiv ? .positive : .negative)
    }
    
    var body: some View {
        
        Form {
            
            Section {
                
                HStack {
                    
                    Spacer()
                    
                    if let uiImage = self.inputImage {
                        
                        VStack {
                            
                            Image(uiImage: uiImage)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .background(Color.white)
                                .clipShape(Circle())
                                .frame(height: 200)
                                .overlay(
                                    
                                    VStack {
                                        
                                        HStack {
                                            
                                            Spacer()
                                            
                                            Button {
                                                self.inputImage = nil
                                            } label: {
                                                
                                                Image(systemName: "xmark.circle.fill")
                                            }
                                            .foregroundColor(.gray)
                                            .background(Color.white)
                                            .font(.system(size: 25))
                                            .clipShape(Circle())
                                        }
                                        
                                        Spacer()
                                    }
                                    .padding()
                                )
                            
                            Button {
                                self.showActionSheet = true
                            } label: {
                                
                                Text("Bild ändern")
                                    .font(.system(size: 17, weight: .regular, design: .default))
                            }
                        }
                    } else {
                        
                        VStack {
                            
                            Image(systemName: "person.fill")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .padding(12)
                                .offset(y: 12)
                                .foregroundColor(.white)
                                .background(Color.gray)
                                .frame(height: 200)
                                .clipShape(Circle())
                                .overlay(Circle().strokeBorder(lineWidth: 5).foregroundColor(.gray))
                            
                            Button {
                                self.showActionSheet = true
                            } label: {
                                
                                Text("Bild hinzufügen (optional)")
                                    .font(.system(size: 17, weight: .regular, design: .default))
                            }
                        }
                    }
                    
                    Spacer()
                }
                .buttonStyle(BorderlessButtonStyle())
            }
            .listRowBackground(Color.colorForIndex(self.selectedColorIndex).opacity(0.4).animation(.spring()))
            
            Section(header: Text("Farbe")) {
                
                HStack {
                    
                    ForEach(0..<5) { index in
                        
                        MiniColor(colorIndex: index, selectedColorIndex: self.$selectedColorIndex)
                        
                        if index != 4 {
                            Spacer()
                        }
                    }
                }
                .buttonStyle(BorderlessButtonStyle())
                .actionSheet(isPresented: self.$showActionSheet) {
                    ActionSheet(
                        title: Text("Foto hinzufügen"),
                        buttons: [
                            .cancel(Text("Abbrechen")),
                            .default(Text("Bild auswählen"), action: {
                                self.sourceType = .photoLibrary
                                self.showImagePicker = true
                            }),
                            .default(Text("Bild aufnehmen"), action: {
                                self.sourceType = .camera
                                self.showImagePicker = true
                            })
                        ]
                    )
                }
                .fullScreenCover(isPresented: $showImagePicker, onDismiss: loadImage) {
                    ImagePicker(image: self.$inputImage, sourceType: self.sourceType)
                        .edgesIgnoringSafeArea(.all)
                }
            }
            .listRowBackground(Color.white)
            
            Section(header: Text("Name")) {
                
                TextField("Vorname", text: self.$firstName)
                    .disableAutocorrection(true)
                
                TextField("Nachname", text: self.$lastName)
                    .disableAutocorrection(true)
                    
                    .alert(isPresented: self.$showMissingInputAlert) {
                        self.missingDataAlert()
                    }
            }
            
            Section(header: Text("Kontakt")) {
                
                TextField("Telefonnummer (optional)", text: self.$phone)
                    .keyboardType(.phonePad)
                
                TextField("Email-Adresse (optional)", text: self.$email)
                    .keyboardType(.emailAddress)
                    .disableAutocorrection(true)
                    .autocapitalization(.none)
            }
            
            Section {
                
                DatePicker("Geburtsdatum", selection: self.$birthDate, in: self.birthDateRange, displayedComponents: .date)
                    .environment(\.locale, Locale.init(identifier: "de_DE"))
                    .onChange(of: self.birthDate) { date in
                        let components = Calendar.current.dateComponents([.year], from: date, to: Date())
                        withAnimation {
                            self.age = components.year ?? 0
                        }
                    }
                
                HStack {
                    
                    Spacer()
                    
                    Text("Alter: \(self.age)")
                }
                .opacity(0.25)
            }
            
            Section(header: Text("Zur Schwangerschaft")) {
                
                DatePicker("ET", selection: self.$et, in: ...(Calendar.current.date(byAdding: .weekOfYear, value: 40, to: Date()) ?? Date()), displayedComponents: .date)
                    .environment(\.locale, Locale.init(identifier: "de_DE"))
                    .onChange(of: self.et) { date in
                        self.pregWeek = date.etString
                    }
                
                HStack {
                    
                    Spacer()
                    
                    Text(self.pregWeek)
                }
                .opacity(0.25)
            }
            
            Section {
                
                HStack {
                    
                    Text("Gravida")
                    
                    Spacer()
                    
                    Picker("                \(self.gravida)", selection: self.$gravida) {
                        ForEach(0..<21) { number in
                            Text("\(number)")
                        }
                    }
                    .pickerStyle(MenuPickerStyle())
                    .animation(.none)
                }
                
                HStack {
                    
                    Text("Para")
                    
                    Spacer()
                    
                    Picker("                \(self.para)", selection: self.$para) {
                        ForEach(0..<21) { number in
                            Text("\(number)")
                        }
                    }
                    .pickerStyle(MenuPickerStyle())
                    .animation(.none)
                }
            }
            
            Section(header: Text("Blutgruppe")) {
                
                Picker("Blutgruppe", selection: self.$bloodGroup) {
                    
                    ForEach(BloodGroup.allCases, id: \.self) { bloodGroup in
                        
                        Text("\(bloodGroup.rawValue)")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                
                Picker("Rhesusfaktor", selection: self.$rhesusFactor) {
                    
                    ForEach(PositiveNegative.allCases, id: \.self) { rhesus in
                        
                        Text("\(rhesus.rawValue)")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
            }
            
            Group {
                
                Section(header: Text("HBS AG")) {
                    
                    Picker("HBS AG", selection: self.$hbsAg) {
                        
                        ForEach(PositiveNegative.allCases.reversed(), id: \.self) { hbs in
                            
                            Text("\(hbs.rawValue)")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("GBS")) {
                    
                    Picker("GBS", selection: self.$gbs) {
                        
                        ForEach(PositiveNegative.allCases.reversed(), id: \.self) { gbs in
                            
                            Text("\(gbs.rawValue)")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("HIV")) {
                    
                    Picker("HIV", selection: self.$hiv) {
                        
                        ForEach(PositiveNegative.allCases.reversed(), id: \.self) { hiv in
                            
                            Text("\(hiv.rawValue)")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
            }
            
            Group {
            
                Section {
                    
                    ZStack {
                        
                        if self.infos.isEmpty {
                            
                            VStack {
                                
                                HStack {
                                    
                                    Text("Infos (optional)")
                                        .padding(.vertical, 10)
                                        .padding(.horizontal, 5)
                                        .opacity(0.25)
                                    
                                    Spacer()
                                }
                                
                                Spacer()
                            }
                        }
                        
                        TextEditor(text: self.$infos)
                            .frame(minHeight: 200)
                    }
                }
                
                Section {
                    
                    HStack {
                        
                        Spacer()
                        
                        Button {
                            self.showDeleteAlert = true
                        } label: {
                            Text("Kontakt löschen")
                                .foregroundColor(.red)
                        }
                        
                        Spacer()
                    }
                }
                .actionSheet(isPresented: self.$showDeleteAlert) {
                    ActionSheet(title: Text("Kontakt wirklich löschen?"),
                                buttons: [
                                    .cancel(Text("Abbrechen")),
                                    .destructive(Text("Kontakt löschen"), action: {
                                        self.deleteContact()
                                    })
                                ])
                }
            }
        }
        .background(
            ZStack {
                Color.white
                
                Color.colorForIndex(self.selectedColorIndex)
                    .opacity(0.4)
            })
        .actionSheet(isPresented: self.$showDismissAlert) {
            ActionSheet(
                title: Text("Willst du die Änderungen wirklich verwerfen?"),
                buttons: [
                    .cancel(Text("Weiter bearbeiten")),
                    .destructive(Text("Änderungen verwerfen"), action: {
                        self.presentationMode.wrappedValue.dismiss()
                    })
                ]
            )
        }
        .edgesIgnoringSafeArea(.all)
        .introspectNavigationController { navigationController in
            navigationController.interactivePopGestureRecognizer?.isEnabled = false
        }
        .onAppear {
            UINavigationBar.setAnimationsEnabled(false)
            DispatchQueue.main.async {
                UINavigationBar.setAnimationsEnabled(true)
            }
        }
        .onWillDisappear {
            UINavigationBar.setAnimationsEnabled(false)
            DispatchQueue.main.async {
                UINavigationBar.setAnimationsEnabled(true)
            }
        }
        .navigationBarBackButtonHidden(true)
        .toolbar {
            
            ToolbarItem(placement: .navigationBarLeading) {
                
                Button(action: {
                    if self.shouldShowDismissAlert {
                        self.showDismissAlert = true
                    } else {
                        self.presentationMode.wrappedValue.dismiss()
                    }
                }) {
                    Text("Abbrechen")
                }
            }
            
            ToolbarItem(placement: .navigationBarTrailing) {
                
                Button(action: {
                    if self.shouldShowMissingInputAlert {
                        self.showMissingInputAlert = true
                    } else {
                        self.addToCoreDataAndDismiss()
                    }
                }) {
                    Text("Aktualisieren")
                }
            }
        }
    }
    
    func loadImage() {
        guard let inputImage = inputImage else { return }
        self.inputImage = inputImage
    }
    
    func missingDataAlert() -> Alert {
        Alert(title: Text("Fehlende Daten"), message: Text("Bitte gebe alle benötigten Daten an."), dismissButton: .cancel(Text("Ok")))
    }
    
    func addToCoreDataAndDismiss() {
        if let image = self.inputImage {
            let turnedImage = image.fixedOrientation()
            self.contact.imageData = turnedImage?.pngData() ?? image.pngData()
        } else {
            self.contact.imageData = nil
        }
        self.contact.firstName = self.firstName
        self.contact.lastName = self.lastName != "" ? self.lastName : nil
        self.contact.birthDate = self.birthDate
        self.contact.phone = self.phone != "" ? self.phone.replacingOccurrences(of: " ", with: "") : nil
        self.contact.email = self.email != "" ? self.email : nil
        self.contact.et = self.et
        self.contact.gravida = Int16(self.gravida)
        self.contact.para = Int16(self.para)
        self.contact.bloodGroup = self.bloodGroup.rawValue
        self.contact.rhesusFactor = self.getBoolForPositiveNegative(self.rhesusFactor.rawValue)
        self.contact.hbsAg = self.getBoolForPositiveNegative(self.hbsAg.rawValue)
        self.contact.gbs = self.getBoolForPositiveNegative(self.gbs.rawValue)
        self.contact.hiv = self.getBoolForPositiveNegative(self.hiv.rawValue)
        self.contact.infos = self.infos != "" ? self.infos : nil
        self.contact.colorIndex = Int16(self.selectedColorIndex)
        PersistenceController.shared.save()
        self.presentationMode.wrappedValue.dismiss()
    }
    
    func deleteContact() {
        self.managedObjectContext.delete(self.contact)
        PersistenceController.shared.save()
        self.showDetailsView = false
    }
    
    func getBoolForPositiveNegative(_ string: String) -> Bool {
        string == "Positiv"
    }
}
