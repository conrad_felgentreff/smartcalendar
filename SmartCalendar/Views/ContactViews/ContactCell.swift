//
//  ContactCell.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 21.06.21.
//

import SwiftUI

struct ContactCell: View {
    
    @ObservedObject var contact: Contact
    @State var showDetailsView = false
    
    var body: some View {
        
        NavigationLink(destination: ContactDetailsView(contact: self.contact, showDetailsView: self.$showDetailsView), isActive: self.$showDetailsView) {
            
            HStack {
                
                if let imageData = contact.imageData {
                    
                    Image(uiImage: UIImage(data: imageData as Data) ?? UIImage())
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .background(Color.colorForIndex(Int(self.contact.colorIndex)))
                        .frame(width: 50, height: 50)
                        .clipShape(Circle())
                        .overlay(Circle().strokeBorder(lineWidth: 2).foregroundColor(Color.colorForIndex(Int(self.contact.colorIndex))))

                } else {
                    
                    Image(systemName: "person.fill")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .padding(3)
                        .offset(y: 3)
                        .foregroundColor(.white)
                        .background(Color.colorForIndex(Int(self.contact.colorIndex)))
                        .frame(width: 50, height: 50)
                        .clipShape(Circle())
                        .overlay(Circle().strokeBorder(lineWidth: 2).foregroundColor(Color.colorForIndex(Int(self.contact.colorIndex))))
                }
                
                HStack(spacing: 0) {
                    
                    Text(contact.firstName)
                        .font(.system(size: 17, weight: self.contact.lastName == nil ? .bold : .regular, design: .default))
                    
                    Text(" ")
                    
                    Text(contact.lastName ?? "")
                        .font(.system(size: 17, weight: .bold, design: .default))
                }
            }
        }
    }
}
