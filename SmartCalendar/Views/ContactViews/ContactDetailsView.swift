//
//  ContactDetailsView.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 19.06.21.
//

import SwiftUI

struct ContactDetailsView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @ObservedObject var contact: Contact
    @Binding var showDetailsView: Bool
    @State var editContact = false
    @State var showNewDocumentView = false
    @State var showMissingAppointmentsAlert = false
    var age: Int {
        let components = Calendar.current.dateComponents([.year], from: contact.birthDate, to: Date())
        return components.year ?? 0
    }
    
    var body: some View {
        
        ZStack {
            
            NavigationLink(destination: EditContactView(contact: self.contact, showDetailsView: self.$showDetailsView), isActive: self.$editContact) {
                Text("")
            }
            
            Form {
                
                Section {
                    
                    HStack {
                        
                        Spacer()
                        
                        if let data = self.contact.imageData, let uiImage = UIImage(data: data) {
                            
                            Image(uiImage: uiImage)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .background(Color.white)
                                .clipShape(Circle())
                                .frame(height: 200)
                        } else {
                            
                            Image(systemName: "person.fill")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .padding(12)
                                .offset(y: 12)
                                .foregroundColor(.white)
                                .background(Color.gray)
                                .frame(height: 200)
                                .clipShape(Circle())
                                .overlay(Circle().strokeBorder(lineWidth: 5).foregroundColor(.gray))
                        }
                        
                        Spacer()
                    }
                    .buttonStyle(BorderlessButtonStyle())
                }
                .listRowBackground(Color.colorForIndex(Int(self.contact.colorIndex)).opacity(0.4))
                
                Group {
                    
                    Section(header: Text("Name")) {
                        
                        Text(self.contact.firstName)
                        
                        if let lastName = self.contact.lastName {
                            Text(lastName)
                        }
                    }
                    
                    Section(header: Text("Telefon")) {
                        
                        if let phone = self.contact.phone {
                            
                            Link(phone, destination: URL(string: "tel:\(phone)") ?? URL(string: "tel:12345")!)
                        } else {
                            
                            Text("Keine Telefonnummer")
                                .foregroundColor(.gray)
                        }
                    }
                    
                    Section(header: Text("Email")) {
                        if let email = self.contact.email {
                            
                            Link(email, destination: URL(string: "mailto:\(email)")!)
                        } else {
                            
                            Text("Keine Emailadresse")
                                .foregroundColor(.gray)
                        }
                    }
                }
                
                if (self.contact.appointmentArray.contains { $0.document != nil }) {
                        
                    NavigationLink(destination: DocumentsForContactView(contact: self.contact)) {
                        
                        Text("Dokumente")
                            .bold()
                    }
                }
                
                Section {
                    
                    HStack {
                     
                        Text("Geburtsdatum")
                        
                        Spacer()
                        
                        Text("\(self.contact.birthDate.dateForDay) (\(self.age))")
                            .foregroundColor(.gray)
                    }
                }
                
                Section(header: Text("Zur Schwangerschaft")) {
                    
                    HStack {
                     
                        Text("ET")
                        
                        Spacer()
                        
                        Text("\(self.contact.et.dateForDay) (\(self.contact.et.etString))")
                            .foregroundColor(.gray)
                    }
                }
                
                Section {
                    
                    HStack {
                        
                        Text("Gravida")
                        
                        Spacer()
                        
                        Text("\(self.contact.gravida)")
                            .foregroundColor(.gray)
                    }
                    
                    HStack {
                        
                        Text("Para")
                        
                        Spacer()
                        
                        Text("\(self.contact.para)")
                            .foregroundColor(.gray)
                    }
                }
                
                Section(header: Text("Blutgruppe")) {
                    
                    HStack {
                     
                        Text("Blutgruppe")
                        
                        Spacer()
                        
                        Text(self.contact.bloodGroup)
                            .foregroundColor(.gray)
                    }
                    
                    HStack {
                     
                        Text("Rhesusfaktor")
                        
                        Spacer()
                        
                        Text("\(PositiveNegative.getPositiveNegative(fromBool: self.contact.rhesusFactor).rawValue)")
                            .foregroundColor(.gray)
                    }
                }
                
                Group {
                    
                    HStack {
                     
                        Text("HBS AG")
                        
                        Spacer()
                        
                        Text("\(PositiveNegative.getPositiveNegative(fromBool: self.contact.hbsAg).rawValue)")
                            .foregroundColor(.gray)
                    }
                    
                    HStack {
                     
                        Text("GBS")
                        
                        Spacer()
                        
                        Text("\(PositiveNegative.getPositiveNegative(fromBool: self.contact.gbs).rawValue)")
                            .foregroundColor(.gray)
                    }
                    
                    HStack {
                     
                        Text("HIV")
                        
                        Spacer()
                        
                        Text("\(PositiveNegative.getPositiveNegative(fromBool: self.contact.hiv).rawValue)")
                            .foregroundColor(.gray)
                    }
                }
                
                Group {
                
                    Section(header: Text("Infos")) {
                        
                        VStack {
                            
                            Text(self.contact.infos ?? "Keine Infos")
                            
                            Spacer()
                        }
                        .frame(minHeight: 150)
                        .foregroundColor(.gray)
                    }
                }
            }
        }
        .background(
            ZStack {
                Color.white
                
                Color.colorForIndex(Int(self.contact.colorIndex))
                    .opacity(0.4)
            })
        .introspectNavigationController { navigationController in
            navigationController.interactivePopGestureRecognizer?.isEnabled = true
        }
        .sheet(isPresented: self.$showNewDocumentView) {
            NewDocumentView(contact: self.contact, show: self.$showNewDocumentView)
        }
        .alert(isPresented: self.$showMissingAppointmentsAlert) {
            self.missingAppointmentsAlert()
        }
        .navigationBarBackButtonHidden(true)
        .toolbar {
            
            ToolbarItem(placement: .navigationBarLeading) {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image(systemName: "chevron.backward")
                }
            }
            
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                
                Button {
                    print(!self.contact.appointmentArray.isEmpty)
                    print((self.contact.appointmentArray.contains { $0.startDate < Date() }))
                    if !self.contact.appointmentArray.isEmpty && (self.contact.appointmentArray.contains { $0.document == nil && $0.startDate < Date() }) {
                        self.showNewDocumentView = true
                    } else {
                        self.showMissingAppointmentsAlert = true
                    }
                } label: {
                    
                    Image(systemName: "doc.badge.plus")
                        .isHidden(self.contact.appointmentArray.isEmpty || !self.contact.appointmentArray.contains { $0.document == nil && $0.startDate < Date() })
                }
                
                Button(action: {
                    self.editContact = true
                }, label: {
                    
                    Image(systemName: "square.and.pencil")
                })
            }
        }
        .edgesIgnoringSafeArea(.bottom)
    }
    
    private func missingAppointmentsAlert() -> Alert {
        Alert(title: Text("Fehlende Termine"), message: Text("Du hast keine vergangenen Termine, welchen du ein Dokument zuordnen könntest."), dismissButton: .cancel(Text("Ok")))
    }
    
    
}
