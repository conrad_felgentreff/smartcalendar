//
//  SideMenu.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 17.06.21.
//

import SwiftUI

struct SideMenu: View {
    
    @Binding var showSideMenu: Bool
    @Binding var index: Int
    
    var body: some View {
        
        VStack(alignment: .leading) {
            
            HStack {
                Button(action: {
                    withAnimation(.easeInOut) {
                        self.showSideMenu = false
                    }
                }, label: {
                    Image(systemName: "xmark")
                        .foregroundColor(.black)
                        .font(.title2)
                })
                
                Spacer()
            }
            .padding(.bottom, 50)
            
            Button(action: {
                withAnimation(.easeInOut) {
                    self.index = 0
                    self.showSideMenu = false
                }
            }, label: {
                
                HStack {
                    
                    Image(systemName: "calendar")
                    
                    Text("Kalender")
                }
                .foregroundColor(.mainColor)
                .font(.system(size: 20, weight: .bold, design: .default))
                .opacity(self.index == 0 ? 1 : 0.5)
            })
            .padding(.vertical)
            
            Button(action: {
                withAnimation(.easeInOut) {
                    self.index = 1
                    self.showSideMenu = false
                }
            }, label: {
                HStack {
                    
                    Image(systemName: "rectangle.stack.person.crop.fill")
                    
                    Text("Kontakte")
                }
                .foregroundColor(.mainColor)
                .font(.system(size: 20, weight: .bold, design: .default))
                .opacity(self.index == 1 ? 1 : 0.5)
            })
            .padding(.vertical)
            
            Spacer()
        }
        .padding()
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.white
                        .ignoresSafeArea())
    }
}
