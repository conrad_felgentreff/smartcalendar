//
//  NotificationHelper.swift
//  SmartCalendar
//
//  Created by Conrad Felgentreff on 25.06.21.
//

import UserNotifications

class NotificationHelper {
    
    static func setNotifications(for appointment: Appointment) {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            if settings.authorizationStatus != .denied {
                let reminderOne = Reminder.getReminderFromInt(Int(appointment.reminderOne))
                if reminderOne != .none {
                    if let notificationDate = Calendar.current.date(byAdding: .minute, value: -reminderOne.minutes, to: appointment.startDate), notificationDate.timeIntervalSinceNow > 0 {
                        NotificationHelper.setNotification(title: "\(appointment.title) (\(appointment.origin.firstName)\(appointment.origin.lastName != nil ? " " + appointment.origin.lastName! : ""))", body: reminderOne.notificationMessage, timerInterval: notificationDate.timeIntervalSinceNow, uuid: appointment.id.uuidString + "reminderOne")
                    }
                } else {
                    removeNotification(withIdentifier: appointment.id.uuidString + "reminderOne")
                }
                
                let reminderTwo = Reminder.getReminderFromInt(Int(appointment.reminderTwo))
                if reminderTwo != .none {
                    if let notificationDate = Calendar.current.date(byAdding: .minute, value: -reminderTwo.minutes, to: appointment.startDate), notificationDate.timeIntervalSinceNow > 0 {
                        NotificationHelper.setNotification(title: "\(appointment.title) (\(appointment.origin.firstName)\(appointment.origin.lastName != nil ? " " + appointment.origin.lastName! : ""))", body: reminderTwo.notificationMessage, timerInterval: notificationDate.timeIntervalSinceNow, uuid: appointment.id.uuidString + "reminderTwo")
                    }
                } else {
                    removeNotification(withIdentifier: appointment.id.uuidString + "reminderTwo")
                }
                print("Notifications set")
            } else {
                print("Permission denied")
            }
        }
    }
    
    private static func setNotification(title: String, body: String, timerInterval: TimeInterval, uuid: String) {
        let content = UNMutableNotificationContent()
        
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default

        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timerInterval, repeats: false)

        let request = UNNotificationRequest(identifier: uuid, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request)
    }
    
    static func removeNotifications(for appointment: Appointment) {
        let identifiers = [ appointment.id.uuidString + "reminderOne", appointment.id.uuidString + "reminderTwo" ]
        for id in identifiers {
            removeNotification(withIdentifier: id)
        }
    }
    
    private static func removeNotification(withIdentifier id: String) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id])
    }
}
